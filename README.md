# Microwatt-test-simulation
# Install GHDL from source

#PCC
# Download powerpc cross compiler and add its bin directory to the path env variable
# mkdir micrw
# mkdir ppc
# cd micrw/ppc

wget https://toolchains.bootlin.com/downloads/releases/toolchains/powerpc64le-power8/tarballs/powerpc64le-power8--glibc--stable-2020.08-1.tar.bz2
tar -xvf powerpc64le-power8--glibc--stable-2020.08-1.tar.bz2
export PATH=$PATH:/home/abhis/micrw/ppc/powerpc64le-power8--glibc--stable-2020.08-1/bin/

# cd 
# cd micrw
# mkdir microwatt-sim
# cd microwatt-sim

git clone https://github.com/antonblanchard/microwatt.git

# cd microwatt
# cd hello_world
make
cd ..
cp hello_world/hello_world.bin main_ram.bin 
./core_tb > /tmp/tb.log
stty sane



